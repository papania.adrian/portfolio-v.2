class Sprite {
    constructor({
        position,
        image,
        frames = {max: 1, hold: 10},
        sprites,
        animate = false,
        rotation = 0,

    })
    {
        this.position = position
        this.image = new Image()
        this.frames = {...frames, val: 0, elasped: 0 }


        this.image.onload = () => {
            this.width = this.image.width / this.frames.max
            this.height = this.image.height
            // console.log(this.width);
            // console.log(this.height);
        }
        this.image.src = image.src
        this.animate = animate
        this.sprites = sprites
        this.opacity = 1
        this.rotation = rotation

    }

    draw() {
        context.save()
        context.translate(
            this.position.x + this.width / 2,
            this.position.y + this.height / 2
            )
        context.rotate(this.rotation)
        context.translate(
            -this.position.x - this.width / 2,
            -this.position.y - this.height / 2
        )
        context.globalAlpha = this.opacity
        context.drawImage(
            this.image,
            // 'Cropping'
            this.frames.val * this.width,
            0,
            this.image.width / this.frames.max,
            this.image.height,
            // 'Actual'
            this.position.x,
            this.position.y,
            this.image.width / this.frames.max,
            this.image.height,
        )
        context.restore()

        if (this.animate) {
            if (this.frames.max > 1) {
                this.frames.elasped++
            }

            if (this.frames.elasped % this.frames.hold === 0) {
                if(this.frames.val < this.frames.max - 1) this.frames.val++
                else this.frames.val = 0
            }
        }
    }
}


// class Monster extends Sprite {
//     constructor({
//         image,
//         frames = {max: 1, hold: 10},
//         sprites,
//         animate = false,
//         rotation = 0,
//         position,
//         isEnemy = false,
//         name,
//         attacks
//     })
//         {
//             super({
//                 image,
//                 frames,
//                 sprites,
//                 animate,
//                 rotation,
//                 position
//             })
//             this.name = name
//             this.health = 100
//             this.isEnemy = isEnemy
//             this.attacks = attacks
//         }
//         faint() {
//             document.querySelector('#dialogueBox').innerHTML =
//             this.name + ' was defeted.'
//             gsap.to(this.position, {
//                 y: this.position.y + 20
//             })
//             gsap.to(this, {
//                 opacity: 0
//             })
//         }
//     attack({ attack, recipient, renderedSprites }) {
//         document.querySelector('#dialogueBox').style.display = 'block'
//         document.querySelector('#dialogueBox').innerHTML = this.name + ' used ' + attack.name

//         let healthBar = '#enemyHealthBar'
//         if (this.isEnemy) healthBar = '#playerHealthBar'

//         recipient.health = recipient.health - attack.damage

//         let rotation = -2.1
//         if (this.isEnemy) rotation = 1

//         switch (attack.name) {
//             case 'Fire':
//                 const fireImage = new Image()
//                 fireImage.src = './img/fire.png'
//                 const Fire = new Sprite({
//                     position: {
//                         x: this.position.x,
//                         y: this.position.y
//                     },
//                     image: fireImage,
//                     frames: {
//                         max: 8,
//                         hold: 10,
//                     },
//                     animate: true,
//                     rotation
//                 })
//                 renderedSprites.splice(1, 0, Fire)

//                 gsap.to(Fire.position, {
//                     x: recipient.position.x,
//                     y: recipient.position.y,
//                     onComplete: () => {
//                         gsap.to(healthBar, {
//                             width: recipient.health - attack.damage + '%'
//                         })

//                         gsap.to(recipient.position, {
//                             x: recipient.position.x + 10,
//                             yoyo: true,
//                             repeat: 5,
//                             duration: .08,
//                         })
//                         gsap.to(recipient, {
//                             opacity: 0,
//                             repeat: 5,
//                             yoyo: true,
//                             duration: .08,
//                         })
//                         renderedSprites.splice(1, 1)
//                     }
//                 })

//                 break
//             case 'Debug':
//                 let tl = gsap.timeline()


//                 let movementDistance = 20
//                 if (this.isEnemy) movementDistance = -20



//                 tl.to(this.position, {
//                     x: this.position.x - movementDistance
//                 })
//                     .to(this.position, {
//                         x: this.position.x + movementDistance * 2,
//                         duration: .1,
//                         onComplete: () => {
//                             // Enemy actually gets hit
//                             gsap.to(healthBar, {
//                                 width: recipient.health - attack.damage + '%',

//                             })

//                             gsap.to(recipient.position, {
//                                 x: recipient.position.x + 10,
//                                 yoyo: true,
//                                 repeat: 5,
//                                 duration: .08,
//                             })
//                             gsap.to(recipient, {
//                                 opacity: 0,
//                                 repeat: 5,
//                                 yoyo: true,
//                                 duration: .08,
//                             })

//                         }
//                     })
//                     .to(this.position, {
//                         x: this.position.x
//                     })
//             break
//             case 'ThrowError':
//                 let tli = gsap.timeline()

//                 let mDistance = 20
//                 if (this.isEnemy) mDistance = -20


//                 tli.to(this.position, {
//                     x: this.position.x - mDistance
//                 })
//                     .to(this.position, {
//                         x: this.position.x + mDistance * 2,
//                         duration: .1,
//                         onComplete: () => {
//                             // Enemy actually gets hit
//                             gsap.to(healthBar, {
//                                 width: recipient.health - attack.damage + '%',

//                             })

//                             gsap.to(recipient.position, {
//                                 x: recipient.position.x + 10,
//                                 yoyo: true,
//                                 repeat: 5,
//                                 duration: .08,
//                             })
//                             gsap.to(recipient, {
//                                 opacity: 0,
//                                 repeat: 5,
//                                 yoyo: true,
//                                 duration: .08,
//                             })

//                         }
//                     })
//                     .to(this.position, {
//                         x: this.position.x
//                     })
//             break
//             case 'Freeze':
//                 let tili = gsap.timeline()

//                 let miDistance = 20
//                 if (this.isEnemy) miDistance = -20


//                 tili.to(this.position, {
//                     x: this.position.x - miDistance
//                 })
//                     .to(this.position, {
//                         x: this.position.x + miDistance * 2,
//                         duration: .1,
//                         onComplete: () => {
//                             // Enemy actually gets hit
//                             gsap.to(healthBar, {
//                                 width: recipient.health - attack.damage + '%',

//                             })

//                             gsap.to(recipient.position, {
//                                 x: recipient.position.x + 10,
//                                 yoyo: true,
//                                 repeat: 5,
//                                 duration: .08,
//                             })
//                             gsap.to(recipient, {
//                                 opacity: 0,
//                                 repeat: 5,
//                                 yoyo: true,
//                                 duration: .08,
//                             })

//                         }
//                     })
//                     .to(this.position, {
//                         x: this.position.x
//                     })
//             break
//             ;
//         }
//     }
// }

class Boundary {
    static width = 64
    static height = 64
    constructor({position}){
        this.position = position
        this.width = 64
        this.height = 64
    }

    draw() {
        context.fillStyle = 'rgba(255, 0, 0, 0.5)';
        context.fillRect(this.position.x, this.position.y, this.width, this.height)
    }
}

const boundaries = []
const offset = {
    x: -1775,
    y:-1650
}

class townMapt {
    static width = 64
    static height =64
    constructor({position}) {
        this.position = position
        this.width = 64
        this.height = 64
    }    

    draw(){
        context.fillStyle = 'rgba(200, 0, 0, 0.5)';
        context.fillRect(this.position.x, this.position.y, this.width, this.height)
    }
}



