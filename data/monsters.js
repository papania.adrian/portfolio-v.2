
const monsters = {
    Pymon: {
        position: {
            x: 250,
            y: 350
        },
        image: {
            src: './img/pymon.png'
        },
        frames: {
            max: 4, hold: 30
        },
        animate : true,
        name: 'Pymon',
        attacks:[attacks.Debug, attacks.Fire]

    },
    ErBug: {
        position: {
            x: 650,
            y: 200
        },
        image: {
            src: './img/eBug.png'
        },
        frames: {
            max: 3, hold: 30
        },
        animate : true,
        isEnemy : true,
        name : 'ErBug',
        attacks: [attacks.ThrowError, attacks.Freeze]
    }
}
