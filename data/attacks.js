const attacks = {
    Debug: {
        name: 'Debug',
        damage: 10,
        type: 'Technical Type',
        color: 'green'
    },
    Fire: {
        name: 'Fire',
        damage: 100,
        type: 'Fire Type',
        color: 'red'
    },
    ThrowError: {
        name: 'ThrowError',
        damage: 10,
        type: 'Technical Type'
    },
    Freeze: {
        name: 'Freeze',
        damage: 30,
        type: 'Technical Type'
    }
}
