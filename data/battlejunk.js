// battle soon to come

const battleZonesMap = []
for (let i = 0; i < battleZonesData.length; i += 70) {
    battleZonesMap.push(battleZonesData.slice(i, 70 + i))
}

const battleZones = []

battleZonesMap.forEach((row, i) => {
    row.forEach((symbol, j) => {
        if (symbol === 6922)
            battleZones.push(
                new Boundary({
                    position: {
                        x: j * Boundary.width + offset.x,
                        y: i * Boundary.height + offset.y,
                    }
                })
            )
    })
})


const battle = {
    initiated: false
}

function animate() {
    battleZones.forEach(battleZone => {
    battleZone.draw()
    if (battle.initiated) return

        // activate battle!!!!!
        if (keys.w.pressed || keys.a.pressed || keys.s.pressed || keys.d.pressed ) {
            for (let i = 0; i < battleZones.length; i++) {
                const battleZone = battleZones[i]
                const overlappingArea =
                (Math.min(player.position.x + player.width, battleZone.position.x + battleZone.width) -
                Math.max(player.position.x, battleZone.position.x)) *
                (Math.min(player.position.y + player.height, battleZone.position.y + battleZone.height) -
                Math.max(player.position.y, battleZone.position.y))


                if (
                    rectangularCollision({
                        retangle1: player,
                        retangle2: battleZone
                    }) &&
                    overlappingArea > player.width * player.height / 2
                    && Math.random() < .01
                )
                {
                console.log('activate battle')
                battle.initiated = true
                gsap.to('#overlappingDiv', {
                    opacity: 1,
                    repeat: 3,
                    yoyo: true,
                    duration: .4,
                    onComplete() {
                        gsap.to('#overlappingDiv', {
                            opacity: 1,
                            duration: .4,
                            onComplete() {
                                initBattle()
                                animateBattle()
                                gsap.to('#overlappingDiv', {
                                    opacity: 0,
                                    duration: .4,
                                })
                            }
                        })
                    }
                })
                break
                }
            }
        }


    })


}