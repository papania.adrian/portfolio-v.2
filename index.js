const canvas = document.querySelector('canvas')
const context = canvas.getContext('2d')





canvas.width = 1024
canvas.height = 576

const collisionsMap = []
for (let i = 0; i < collisions.length; i += 70) {
    collisionsMap.push(collisions.slice(i, 70 + i))
}

const townMapmap = []
for (let i = 0; i < townMapData.length; i+= 70){
    townMapmap.push(townMapData.slice(i, 70 + i))
}

const townMap = []
townMapmap.forEach((row, i) => {
    row.forEach((symbol, j) => {
        if (symbol === 5981)
            townMap.push(
                new Boundary({
                    position: {
                        x: j * Boundary.width + offset.x,
                        y: i * Boundary.height + offset.y,
                    }
                })
            )
    })
})









collisionsMap.forEach((row, i) => {
    row.forEach((symbol, j) => {
        if (symbol === 6922)
            boundaries.push(
                new Boundary({
                    position: {
                        x: j * Boundary.width + offset.x,
                        y: i * Boundary.height + offset.y,
                    }
                })
            )
    })
})





const image = new Image()
image.src = './img/Portfolio village v.1.png'


const playerDownImage = new Image()
playerDownImage.src = './img/Odderf Walk down v.2.png'

const playerUpImage = new Image()
playerUpImage.src = './img/Odderf Walk up v.2.png'

const playerLeftImage = new Image()
playerLeftImage.src = './img/Odderf Walk left v.2.png'

const playerRightImage = new Image()
playerRightImage.src = './img/Odderf Walk right v.2.png'




const player = new Sprite({
    position: {
        x: canvas.width / 2 - 384 / 4 / 2,
        y: canvas.height / 2 - 64 / 2
    },
    image: playerDownImage,
    frames: {max: 6, hold: 10},
    sprites: {
        up: playerUpImage,
        down: playerDownImage,
        left: playerLeftImage,
        right: playerRightImage,

    }
})

const background = new Sprite({
    position: {
        x: offset.x,
        y: offset.y,
    },
    image: image
})

const keys = {
    w: {
        pressed: false
    },
    a: {
        pressed: false
    },
    s: {
        pressed: false
    },
    d: {
        pressed: false
    },
}



const movables = [background, ...boundaries, ...townMap]
function rectangularCollision({ retangle1, retangle2}) {
    return (
        retangle1.position.x + retangle1.width >= retangle2.position.x &&
        retangle1.position.x <= retangle2.position.x + retangle2.width &&
        retangle1.position.y <= retangle2.position.y + retangle2.height &&
        retangle1.position.y + retangle1.height >= retangle2.position.y
    )
}


function animate() {
    window.requestAnimationFrame(animate)
    background.draw()

    boundaries.forEach(boundary => {
        boundary.draw()
    })

    townMap.forEach(townMap => {
        townMap.draw()
    })

    player.draw()

    let moving = true
    player.animate = false
    
    let tm = document.getElementById('townMap')
    for (let i = 0; i < townMap.length; i++) {
        const townMapp = townMap[i]
        
        if (
            rectangularCollision({
                retangle1: player,
                retangle2: townMapp
                
            })
            && lastKey == 'w'
        )
        {
            initinteraction()

        }

    }







    if (keys.w.pressed && lastKey === 'w') {
        player.animate = true
        player.image = player.sprites.up
        for (let i = 0; i < boundaries.length; i++) {
            const boundary = boundaries[i]
            if (
                rectangularCollision({
                    retangle1: player,
                    retangle2: {...boundary, position: {
                        x: boundary.position.x,
                        y: boundary.position.y + 3
                    }}
                })
            )
            {
            moving = false
            break
            }
        }





        if(moving)
            movables.forEach((movable) => {
                movable.position.y += 3 })}
    else if (keys.a.pressed && lastKey == 'a') {
        tm.style.display = 'none'
        player.animate = true
        player.image = player.sprites.left
        for (let i = 0; i < boundaries.length; i++) {
            const boundary = boundaries[i]
            if (
                rectangularCollision({
                    retangle1: player,
                    retangle2: {...boundary, position: {
                        x: boundary.position.x + 3,
                        y: boundary.position.y
                    }}
                })
            )
            {
            moving = false
            break
            }
        }
        if(moving)
        movables.forEach((movable) => {
            movable.position.x += 3 })}
    else if (keys.s.pressed && lastKey == 's') {
        tm.style.display = 'none'
        player.animate = true
        player.image = player.sprites.down
        for (let i = 0; i < boundaries.length; i++) {
            const boundary = boundaries[i]
            if (
                rectangularCollision({
                    retangle1: player,
                    retangle2: {...boundary, position: {
                        x: boundary.position.x,
                        y: boundary.position.y - 3
                    }}
                })
            )
            {
            moving = false
            break
            }
        }
        if(moving)
        movables.forEach((movable) => {
            movable.position.y -= 3 })}
    else if (keys.d.pressed && lastKey == 'd') {
        tm.style.display = 'none'
        player.animate = true
        player.image = player.sprites.right
        for (let i = 0; i < boundaries.length; i++) {
            const boundary = boundaries[i]
            if (
                rectangularCollision({
                    retangle1: player,
                    retangle2: {...boundary, position: {
                        x: boundary.position.x - 3,
                        y: boundary.position.y
                    }}
                })
            )
            {
            moving = false
            break
            }
        }
        if(moving)
        movables.forEach((movable) => {
            movable.position.x -= 3 })}
}


let lastKey = ''
window.addEventListener('keydown', (e) => {
    switch (e.key) {
        case 'w':
            keys.w.pressed = true
            lastKey = 'w'
            break
        case 'a':
            keys.a.pressed = true
            lastKey = 'a'
            break
        case 's':
            keys.s.pressed = true
            lastKey = 's'
            break
        case 'd':
            keys.d.pressed = true
            lastKey = 'd'
            break
    }
})

window.addEventListener('keyup', (e) => {
    switch (e.key) {
        case 'w':
            keys.w.pressed = false
            break
        case 'a':
            keys.a.pressed = false
            break
        case 's':
            keys.s.pressed = false
            break
        case 'd':
            keys.d.pressed = false
            break
    }
})

